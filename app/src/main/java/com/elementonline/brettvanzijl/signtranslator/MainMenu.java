package com.elementonline.brettvanzijl.signtranslator;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.io.File;


public class MainMenu extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
        //clearGestures();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void recordGesture(View view){
        Intent intent = new Intent(this, RecordName.class);
        startActivity(intent);
    }

    public void translateGestures(View view){
        Intent intent = new Intent(this, TranslateGestures.class);
        startActivity(intent);
    }

    /**
     * Clear the stored Gestures
     */
    public void clearGestures(){
        File mydir = this.getDir("Gestures", Context.MODE_PRIVATE);
        File[] files = mydir.listFiles();
        for (File file : files){
            file.delete();
        }
        Toast toast =  Toast.makeText(this, "Gestures Cleared", Toast.LENGTH_SHORT);
        toast.show();
    }
}

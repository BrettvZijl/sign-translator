package com.elementonline.brettvanzijl.signtranslator;

import android.content.Context;
import android.speech.tts.TextToSpeech;
import android.view.View;
import android.widget.Toast;

import com.leapmotion.leap.Controller;
import com.leapmotion.leap.Finger;
import com.leapmotion.leap.FingerList;
import com.leapmotion.leap.Frame;
import com.leapmotion.leap.Hand;
import com.leapmotion.leap.HandList;
import com.leapmotion.leap.Listener;
import com.leapmotion.leap.Vector;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Brett Van Zijl on 2015-07-22.
 */
public class Recognizer extends Listener {
    ArrayList<Gesture> trainGestures;   //The array of recorded gestures that will be compared to in order to recognize the gesture made.
    Gesture gesture;    //the gesture that will be recorded and matched to the trained gestures.
    Boolean recording = false;
    int frameCount = 0;
    int minGestureFrames = 5;	// The minimum number of recorded frames considered as possibly containing a recognisable gesture
    int minRecordingVelocity = 60; // The minimum velocity a frame needs to clock in at to trigger gesture recording, or below to stop gesture recording (by default)
    int maxRecordingVelocity = 30;	// The maximum velocity a frame can measure at and still trigger pose recording, or above which to stop pose recording (by default)
    Boolean stopRecording = false;  //says if recording should be stopped
    double hitThreshold = 0.75;     // The correlation output value above which a gesture is considered recognized. Raise this to make matching more strict
    long lastHit = 0;	// The timestamp at which the last gesture was identified (recognized or not), used when calculating downtime
    int downtime = 1000;	// The number of milliseconds after a gesture is identified before another gesture recording cycle can begin
    TextToSpeech tts;   //the Text to Speech engine that will vocalise the matched gesture text

    public Recognizer(Context context) throws IOException {
        trainGestures = new ArrayList<Gesture>();

        String fileName= "Gestures\\";
        ObjectInputStream fInput = null;
        Gesture trained=null;

        File mydir = context.getDir("Gestures", Context.MODE_PRIVATE);
        File[] files = mydir.listFiles();
        if (files.length == 0){
            Toast toast = Toast.makeText(context, "Gestures is Empty", Toast.LENGTH_SHORT);
            toast.show();
        }
        for (File file : files){
            //String name = file.getName();

            try{
                fInput = new ObjectInputStream(new BufferedInputStream(new FileInputStream(file)));
                trained = (Gesture) fInput.readObject();
                //Normalize template gesture.
                TemplateMatcher template = new TemplateMatcher();
                trained.points = template.resample(trained.points, 44);
                trained.points = template.scale(trained.points);
                trained.points = template.translateTo(trained.points, new Point(0.0,0.0,0.0));

                //add template to trained gestures.
                trainGestures.add(trained);
                Toast toast = Toast.makeText(context, "Gesture "+trained.name+" Loaded", Toast.LENGTH_SHORT);
                toast.show();

            } catch (ClassNotFoundException ex) {
                Logger.getLogger(Recognizer.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                fInput.close();
            }

            System.out.println("Loaded " + trained.name + " Gesture");

        }

        //initialise the TTS engine
        tts = new TextToSpeech(context, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status != TextToSpeech.ERROR) {
                    tts.setLanguage(Locale.UK);
                }
            }
        });

    }

    @Override
    public void onConnect(Controller controller) {
        System.out.println("Connected");
        //recording = true;
        //System.out.println("Recording...");
    }

    @Override
    public void onFrame(Controller controller) {
        Frame frame = controller.frame();
        //System.out.println("in frame...");

        if ((new Date().getTime() - lastHit) < downtime){
            //System.out.println("waiting");
            return;
        }


        if (recordableFrame(frame, minRecordingVelocity)){

            /*
             * If this is the first frame in a gesture, we clean up some running values and fire the 'started-recording' event.
             */
            if (!recording) {
                gesture = new Gesture();
                recording = true;
                frameCount = 0;
            }


            //System.out.println("in frame... " + Long.toString(frame.id()));
            recordFrame(frame);
            frameCount++;
            //System.out.println("Recording Frame...");
        }else if(recording){
            /*
             * If the frame should not be recorded but recording was active, then we deactivate recording and check to see if enough
             * frames have been recorded to qualify for gesture recognition.
             */
            recording = false;
            /*
             * As soon as we're no longer recording, we fire the 'stopped-recording' function.
             */


            if (frameCount >= minGestureFrames){

                TemplateMatcher matcher = new TemplateMatcher();
                RecognizerResults results = matcher.recognizer(gesture, trainGestures);
                System.out.println("MATCHING");

                if(results.mScore > 0.0){
                    if (results.mScore >= hitThreshold){
                        System.out.println("MATCH: " + results.mName);
                        CharSequence result = results.mName;
                        tts.speak(result,TextToSpeech.QUEUE_ADD,null,null);

                    }else{
                        System.out.println("NO MATCH");
                    }
                }
                lastHit = new Date().getTime();
            }
        }
    }

    /**
     * This function is called for each frame during gesture recording, and it is responsible for adding values in frames using the provided
     * recordVector and recordValue functions (which accept a 3-value numeric array and a single numeric value respectively).
     */
    public void recordFrame(Frame frame) {
        //HandList hands = frame.hands();
        //gesture.hands = hands;

        HandList hands = frame.hands();
        int handCount = hands.count();

        Hand hand;
        Finger finger;
        FingerList fingers;
        int fingerCount;

        int l = handCount;
        for (int i = 0; i < l; i++) {   //for each hand in the frame
            hand = hands.get(i);

            recordPoint(hand.stabilizedPalmPosition());     //record the palm position

            fingers = hand.fingers();
            fingerCount = fingers.count();

            int k = fingerCount;
            for (int j = 0; j < k; j++) {   //for each finger in the hand
                finger = fingers.get(j);
                recordPoint(finger.stabilizedTipPosition());	//record the fingertip position.
            }
        }
        System.out.println("Recording Frame..." + Long.toString(frame.id()));
    }

    /**
     * This function records a point to the gesture
     * @param val
     */
    public void recordPoint(Vector val){
        double x,y,z;
        //NaNs are replaced with 0.0, though they shouldn't occur!
        if (Double.isNaN(val.getX())){
            x=0.0;
        }else{
            x=val.getX();
        }

        if (Double.isNaN(val.getY())){
            y=0.0;
        }else{
            y=val.getY();
        }

        if (Double.isNaN(val.getZ())){
            z=0.0;
        }else{
            z=val.getZ();
        }

        Point point = new Point(x, y, z);
        gesture.add(point);
    }

    /**
     * This function returns TRUE if the provided frame should trigger recording and FALSE if it should stop recording.
     *
     * Of course, if the system isn't already recording, returning FALSE does nothing, and vice versa.. So really it returns
     * whether or not a frame may possibly be part of a gesture.
     *
     * By default this function makes its decision based on one or more hands or fingers in the frame moving faster than the
     * configured minRecordingVelocity, which is provided as a second parameter.
     *
     * @param frame
     * @param min
     *
     * @returns {Boolean}
     */
    public Boolean recordableFrame(Frame frame, int min){

        HandList hands = frame.hands();
        int j;
        Hand hand;
        FingerList fingers;
        double palmVelocity;
        double tipVelocity;
        Boolean poseRecordable = false;

        int l=hands.count();
        for(int i=0; i<l; i++){
            hand= hands.get(i);

            Vector palmVelocitys = hand.palmVelocity();
            palmVelocity = Math.max(Math.abs(palmVelocitys.getX()), Math.abs(palmVelocitys.getY()));
            palmVelocity = Math.max(palmVelocity, Math.abs(palmVelocitys.getZ()));

            /*
             * We return true if there is a hand moving above the minimum recording velocity
             */
            if (palmVelocity >= min){return true;}

            fingers = hand.fingers();

            int k = fingers.count();
            for (j=0; j<k; j++){
                Vector tipVelocitys = fingers.get(j).tipVelocity();
                tipVelocity = Math.max(Math.abs(tipVelocitys.getX()), Math.abs(tipVelocitys.getY()));
                tipVelocity = Math.max(tipVelocity, Math.abs(tipVelocitys.getZ()));

                /*
                 * Or if there's a finger tip moving above the minimum recording velocity
                 */
                if (tipVelocity >= min) { return true; }
            }
        }


        return false;
    }


    /**
     * Set the stopRecording variable to true, to tell the main program that the
     * gesture has been recorded and so no longer needs to record anymore gestures.
     *
     */
    public void stopRecording(){
        stopRecording=true;
    }
}

package com.elementonline.brettvanzijl.signtranslator;

import android.content.Context;
import android.view.View;

import com.leapmotion.leap.Controller;

import java.io.IOException;

/**
 * Created by Brett Van Zijl on 2015-07-22.
 */
public class RecognizeGestures {
    Controller controller;
    Recognizer listener;

    public void run(Context context) throws IOException, InterruptedException{
        listener = new Recognizer(context);
        controller = new Controller();
        controller.setPolicy(Controller.PolicyFlag.POLICY_OPTIMIZE_HMD); //optimized for head mounted display (LM being in vertical orientation)
        controller.addListener(listener);

     /* while (!listener.stopRecording){
          //System.out.println("Recording..." + listener.stopRecording.toString());
       }*/

        // Keep this process running until Enter is pressed
        //This is the only method that I could find that allows the process to run without causing issues. while(true) statements did not work.
        /*try {
            System.in.read();
        } catch (IOException e) {
            e.printStackTrace();
        }*/



    }

    public void stop(){
        controller.removeListener(listener);
        System.out.println("Removed Listener");
    }
}

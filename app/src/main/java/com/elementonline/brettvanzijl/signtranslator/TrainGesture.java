package com.elementonline.brettvanzijl.signtranslator;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.Toast;

import com.leapmotion.leap.Controller;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Brett Van Zijl on 2015-07-22.
 */
public class TrainGesture {
    Controller controller;
    Trainer listener;

    public void run(Context context, String input) throws IOException, InterruptedException{

        String name = input;

        int countDown = 3;
        while (countDown>0){
            System.out.println("Recording Gesture in ... " + Integer.toString(countDown));
            Toast toast = Toast.makeText(context, "Recording Gesture in ... " + Integer.toString(countDown), Toast.LENGTH_SHORT);
            toast.show();
            countDown--;
            Thread.sleep(1000);
        }

        listener = new Trainer(name, context);
        controller = new Controller();
        controller.setPolicy(Controller.PolicyFlag.POLICY_OPTIMIZE_HMD);//optimized for head mounted display (LM being in vertical orientation)
        controller.addListener(listener);

     /* while (!listener.stopRecording){
          //System.out.println("Recording..." + listener.stopRecording.toString());
       }*/

        // Keep this process running until Enter is pressed
        //This is the only method that I could find that allows the process to run without causing issues. while(true) statements did not work.
        /*try {

            System.in.read();
        } catch (IOException e) {
            e.printStackTrace();
        }*/

        //controller.removeListener(listener);
        //System.out.println("Removed Listener");

    }

    public void stop(){
        controller.removeListener(listener);
    }
}
